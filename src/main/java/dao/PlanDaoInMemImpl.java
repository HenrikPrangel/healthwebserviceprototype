package dao;

import dao.Interfaces.PlanDao;
import model.plan.DayTimeFrame;
import model.plan.enums.Days;
import model.plan.enums.ElementTypes;
import model.plan.Plan;
import model.plan.PlanDay;
import model.plan.PlanElement;
import model.plan.PlanSection;
import org.springframework.stereotype.Repository;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static java.util.stream.Collectors.toList;

@Repository
public class PlanDaoInMemImpl implements PlanDao {

	private static List<Plan> allWebAppPlans = new ArrayList<>();

	static {
		Plan plan1 = new Plan(1, "Vegan", true, 1);
		plan1.setDescription("An absolute delight of vegan meals");

		PlanElement element1 = new PlanElement(1, ElementTypes.Food, 1, "Banana", 2.0);
		PlanElement element2 = new PlanElement(2, ElementTypes.Food, 2, "Kiwi", 2.0);
		PlanElement element3 = new PlanElement(3, ElementTypes.Food, 3, "Starwberry", 2.0);
		PlanElement element4 = new PlanElement(4, ElementTypes.Food, 4, "Apple", 2.0);
		PlanElement element5 = new PlanElement(5, ElementTypes.Food, 1, "Banana", 1.0);
		PlanElement element6 = new PlanElement(6, ElementTypes.Food, 5, "Banana", 2.0);
		PlanElement element7 = new PlanElement(7, ElementTypes.Food, 6, "Banana", 2.0);
		PlanElement element8 = new PlanElement(8, ElementTypes.Food, 2, "Kiwi", 3.0);
		PlanElement element9 = new PlanElement(9, ElementTypes.Food, 7, "Banana", 2.0);
		PlanElement element10 = new PlanElement(10, ElementTypes.Training, 22, "Push ups", 2.0);
		PlanElement element11 = new PlanElement(11, ElementTypes.Training, 23, "Light jog", 3.0);
		PlanElement element12 = new PlanElement(12, ElementTypes.Training, 74, "Squats", 2.0);

		PlanDay mon = new PlanDay(1,1, Days.Monday);

		DayTimeFrame breakfest = new DayTimeFrame(1,"Breakfest");
		breakfest.setPlanElementList(new ArrayList(Arrays.asList(element1,element2,element3)));
		DayTimeFrame supper = new DayTimeFrame(2, "Supper");
		supper.setPlanElementList(new ArrayList(Arrays.asList(element4,element5,element6, element7)));
		DayTimeFrame lunch = new DayTimeFrame(3, "Lunch");
		lunch.setPlanElementList(new ArrayList(Arrays.asList(element8,element9)));
		DayTimeFrame Training = new DayTimeFrame(4, "Training");
		Training.setPlanElementList(new ArrayList(Arrays.asList(element10,element11,element12)));

		mon.setDayTimeFrameList(new ArrayList(Arrays.asList(breakfest,supper,lunch,Training)));

		PlanDay tue = new PlanDay(2,1,Days.Tuesday);
		tue.setDayTimeFrameList(new ArrayList(Arrays.asList(breakfest,supper,lunch,Training)));
		PlanDay wed = new PlanDay(3,1,Days.Wednesday);
		wed.setDayTimeFrameList(new ArrayList(Arrays.asList(breakfest,supper,lunch,Training)));
		PlanDay thu = new PlanDay(4,1,Days.Thursday);
		thu.setDayTimeFrameList(new ArrayList(Arrays.asList(breakfest,supper,lunch,Training)));
		PlanDay fri = new PlanDay(5,1,Days.Friday);
		fri.setDayTimeFrameList(new ArrayList(Arrays.asList(breakfest,supper,lunch)));
		PlanDay sat = new PlanDay(6,1,Days.Saturday);
		sat.setDayTimeFrameList(new ArrayList(Arrays.asList(breakfest,supper,lunch,Training)));
		PlanDay sun = new PlanDay(7,1,Days.Sunday);
		sun.setDayTimeFrameList(new ArrayList(Arrays.asList(breakfest,supper,lunch,Training)));

		PlanSection planSection1 = new PlanSection(1, 1, "week 1");
		ArrayList<PlanSection> sectionListforPlan1 = new ArrayList<>();
		planSection1.setPlanDayList(new ArrayList(Arrays.asList(mon, tue, wed, thu, fri, sat, sun)));
		sectionListforPlan1.add(planSection1);
		plan1.setPlanSectionList(sectionListforPlan1);



		Plan plan2 = new Plan(2, "Meat Lover", true, 2);
		plan2.setDescription("For all haunchy meat lovers");

		PlanElement element13 = new PlanElement(9, ElementTypes.Food, 10, "Red meat", 2.0);
		PlanElement element14 = new PlanElement(10, ElementTypes.Food, 20, "Bacon", 2.0);
		PlanElement element15 = new PlanElement(11, ElementTypes.Food, 30, "Chicken", 2.0);
		PlanElement element16 = new PlanElement(12, ElementTypes.Food, 40, "Dark chocolate", 2.0);
		PlanElement element17 = new PlanElement(13, ElementTypes.Food, 10, "Red meat", 1.0);
		PlanElement element18 = new PlanElement(14, ElementTypes.Food, 50, "Banana", 2.0);
		PlanElement element19 = new PlanElement(15, ElementTypes.Food, 60, "Peanuts", 2.0);
		PlanElement element20 = new PlanElement(16, ElementTypes.Food, 20, "Bacon", 3.0);

		PlanDay mon1 = new PlanDay(10,1, Days.Monday);

		DayTimeFrame breakfast1 = new DayTimeFrame(10,"Breakfast");
		breakfast1.setPlanElementList(new ArrayList(Arrays.asList(element13,element14)));
		DayTimeFrame supper1 = new DayTimeFrame(20, "Supper");
		supper1.setPlanElementList(new ArrayList(Arrays.asList(element15,element16,element17, element18)));
		DayTimeFrame lunch1 = new DayTimeFrame(3, "Lunch");
		lunch1.setPlanElementList(new ArrayList(Arrays.asList(element19,element20)));
		DayTimeFrame Training1 = new DayTimeFrame(4, "Training");
		Training1.setPlanElementList(new ArrayList(Arrays.asList(element10)));

		mon1.setDayTimeFrameList(new ArrayList(Arrays.asList(breakfast1,supper1,lunch1,Training1)));

		PlanDay tue1 = new PlanDay(2,1,Days.Tuesday);
		tue1.setDayTimeFrameList(new ArrayList(Arrays.asList(breakfest,supper1,lunch1,Training)));
		PlanDay wed1 = new PlanDay(3,1,Days.Wednesday);
		wed1.setDayTimeFrameList(new ArrayList(Arrays.asList(breakfast1,supper,lunch1)));
		PlanDay thu1 = new PlanDay(4,1,Days.Thursday);
		thu1.setDayTimeFrameList(new ArrayList(Arrays.asList(breakfast1,supper1,lunch1,Training1)));
		PlanDay fri1 = new PlanDay(5,1,Days.Friday);
		fri1.setDayTimeFrameList(new ArrayList(Arrays.asList(breakfast1,supper1,lunch1)));
		PlanDay sat1 = new PlanDay(6,1,Days.Saturday);
		sat1.setDayTimeFrameList(new ArrayList(Arrays.asList(breakfast1,supper1,lunch1,Training)));
		PlanDay sun1 = new PlanDay(7,1,Days.Sunday);
		sun1.setDayTimeFrameList(new ArrayList(Arrays.asList(breakfast1,supper1,lunch1)));

		PlanSection planSection10 = new PlanSection(1, 1, "week 1");
		ArrayList<PlanSection> sectionListforPlan10 = new ArrayList<>();
		planSection10.setPlanDayList(new ArrayList(Arrays.asList(mon1, tue1, wed1, thu1, fri1, sat1, sun1)));
		sectionListforPlan10.add(planSection10);
		plan2.setPlanSectionList(sectionListforPlan10);

		Plan plan3 = new Plan(3, "Many Peanuts", true, 1);
		plan3.setDescription("Consist only of peanuts");

		Plan plan4 = new Plan(4, "Fishy", true, 1);
		plan4.setDescription("For those that love fish, this is the one");

		Plan plan5 = new Plan(5, "Dairy", false, 2);
		plan5.setDescription("Such a dairy world");

		Plan plan6 = new Plan(6, "Test plan 1", true, 6);
		plan6.setDescription("This is a samle description");

		Plan plan7 = new Plan(7, "Test plan 2", true, 6);
		plan7.setDescription("This is a samle description");

		Plan plan8 = new Plan(8, "Test plan 3", true, 6);
		plan8.setDescription("This is a samle description");

		Plan plan9 = new Plan(9, "Test plan 4", true, 6);
		plan9.setDescription("This is a samle description");

		Plan plan10 = new Plan(10, "Test plan 5", true, 6);
		plan10.setDescription("This is a samle description");

		Plan plan11 = new Plan(11, "Test plan 6", true, 6);
		plan11.setDescription("This is a samle description");

		allWebAppPlans = new ArrayList(Arrays.asList(plan1, plan2, plan3, plan4, plan5, plan6, plan7, plan8, plan9, plan10, plan11));
	}

	@Override
	public Plan getPlanById(Integer id, Integer userId) {
		return allWebAppPlans.stream().filter( plan ->  (plan.getUserId().equals(userId) || plan.getAcceccibleToPublic()) && plan
				.getId().equals(id))
				.collect(toList()).get(0);
	}

	@Override
	public List<Plan> getAllUserPlans(Integer userId) {
		return allWebAppPlans.stream().filter( plan -> plan.getUserId().equals(userId)).collect(toList());
	}

	@Override
	public List<Plan> getAllPublicPlans() {
		return allWebAppPlans.stream().filter(Plan::getAcceccibleToPublic).collect(toList());
	}

	@Override
	public Plan insertOrUpdatePlan(Plan plan, Integer userId) {
		if(plan.getId() == null) {
			Integer higehstId = 0;
			for (Plan tPlan: allWebAppPlans) {
				if(tPlan.getId() > higehstId) {
					higehstId = tPlan.getId();
				}
			}
			plan.setUserId(userId);
			plan.setId(higehstId + 1);
			allWebAppPlans.add(plan);
		} else {
			throw new NotImplementedException();
		}

		return plan;
	}

	@Override
	public void deletePlan(Integer id, Integer userId) {
		List<Plan> filtered = allWebAppPlans.stream().filter(plan -> !(plan.getUserId().equals(userId)
				&& plan.getId().equals(id))).collect(toList());
		allWebAppPlans = filtered;
	}
}
