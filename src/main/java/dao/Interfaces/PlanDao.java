package dao.Interfaces;

import model.plan.Plan;

import java.util.List;

public interface PlanDao {

	Plan getPlanById(Integer id, Integer userId);

	List<Plan> getAllUserPlans(Integer userId);

	List<Plan> getAllPublicPlans();

	Plan insertOrUpdatePlan(Plan plan, Integer userId);

	void deletePlan(Integer id, Integer userId);

}
