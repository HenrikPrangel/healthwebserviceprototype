package user;

public class User {

    private Integer id;

    private String userName;

    private String password;

    private String name;

    private Integer activePlan;

    public User() {
    }

    public User(String userName, String name, Integer id) {
        this.userName = userName;
        this.name = name;
        this.id = id;
    }

    public User(String userName, String password,Integer id, Integer activePlan) {
        this.id = id;
        this.userName = userName;
        this.password = password;
        this.activePlan = activePlan;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getActivePlan() {
        return activePlan;
    }

    public void setActivePlan(Integer activePlan) {
        this.activePlan = activePlan;
    }

    @Override
    public String toString() {
        return "User{" +
                "userName='" + userName + '\'' +
                ", name='" + name + '\'' +
                '}';
    }
}
