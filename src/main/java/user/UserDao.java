package user;

import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static java.util.stream.Collectors.toList;

@Repository
public class UserDao {

	private static List<User> users = new ArrayList<>();
	private static User activeUser = null;

	static {
		User user = new User("user", "Test User 1", 1, 1);
		User user1 = new User("user1", "Test User 2", 2);
		User user2 = new User("user2", "Test User 3", 3);
		users.addAll(Arrays.asList(user, user1, user2));
	}

	public User login(String userName) {
		User user = getUserByUserName(userName);
		activeUser = user;
		return user;
	}

	public User getActiveUser() {
		return activeUser;
	}

	public User setActiveUserActivePlan(Integer planId) {
		System.out.println(planId);
		activeUser.setActivePlan(planId);
		return activeUser;
	}

	public User getUserByUserName(String userName) {
		System.out.println(users);
		return users.stream().filter(user -> user.getUserName().equals(userName)).collect(toList()).get(0);
	}

}
