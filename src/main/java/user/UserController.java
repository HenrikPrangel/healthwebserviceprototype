package user;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpSession;
import java.security.Principal;

@RestController
@CrossOrigin("*")
public class UserController {

    @Autowired
    private UserDao dao;

    @PostMapping("/login")
    public User counter(@RequestBody User user) {
        return dao.login(user.getUserName());
    }


    @PostMapping("/setActivePlanForUser/{planId}")
    public User counter(@PathVariable("planId") Integer planId) {
        return dao.setActiveUserActivePlan(planId);
    }

}