package controller;

import dao.Interfaces.PlanDao;
import model.plan.Plan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import user.UserDao;
import java.util.List;

@RestController
@CrossOrigin("*")
public class PlanController {

	private static final String PLAN_URL = "plan";

	@Autowired
	private PlanDao planDao;

	@Autowired
	private UserDao userDao;

	@GetMapping(PLAN_URL)
	public List<Plan> getAllUserPlans() {
		if (userDao.getActiveUser() == null) {
			return null;
		}
		return planDao.getAllUserPlans(userDao.getActiveUser().getId());
	}

	@GetMapping(PLAN_URL + "/public")
	public List<Plan> getAllPublicPlans() {
		return planDao.getAllPublicPlans();
	}

	@GetMapping(PLAN_URL + "/{id}")
	public Plan getPlanById(@PathVariable("id") Integer id) {
		Integer userId = userDao.getActiveUser().getId();

		return planDao.getPlanById(id, userId);
	}

	@DeleteMapping(PLAN_URL + "/{id}")
	public void deletePlanById(@PathVariable("id") Integer id) {
		Integer userId = userDao.getActiveUser().getId();

		planDao.deletePlan(id, userId);
	}

	@PostMapping(PLAN_URL)
	public Plan insertOrUpdatePlan(@RequestBody Plan plan) {
		Integer userId = userDao.getActiveUser().getId();
		return planDao.insertOrUpdatePlan(plan, userId);
	}

}
