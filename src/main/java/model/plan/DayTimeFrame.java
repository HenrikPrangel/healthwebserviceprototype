package model.plan;

import java.util.List;

public class DayTimeFrame {

	private Integer dayTimeFrameId;
	private String dayTimeFrameName;
	private List<PlanElement> planElementList;

	public DayTimeFrame() {
	}

	public DayTimeFrame(Integer dayTimeFrameId, String dayTimeFrameName) {
		this.dayTimeFrameId = dayTimeFrameId;
		this.dayTimeFrameName = dayTimeFrameName;
	}

	public Integer getDayTimeFrameId() {
		return dayTimeFrameId;
	}

	public void setDayTimeFrameId(Integer dayTimeFrameId) {
		this.dayTimeFrameId = dayTimeFrameId;
	}

	public String getDayTimeFrameName() {
		return dayTimeFrameName;
	}

	public void setDayTimeFrameName(String dayTimeFrameName) {
		this.dayTimeFrameName = dayTimeFrameName;
	}

	public List<PlanElement> getPlanElementList() {
		return planElementList;
	}

	public void setPlanElementList(List<PlanElement> planElementList) {
		this.planElementList = planElementList;
	}
}
