package model.plan;

import java.util.List;

public class Plan {

	private Integer id;
	private String name;
	private String description;
	private Boolean acceccibleToPublic;
	private Integer userId;
	private List<PlanSection> planSectionList;

	public Plan() {}

	public Plan(Integer id, String name, Boolean acceccibleToPublic, Integer userId) {
		this.id = id;
		this.name = name;
		this.acceccibleToPublic = acceccibleToPublic;
		this.userId = userId;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Boolean getAcceccibleToPublic() {
		return acceccibleToPublic;
	}

	public void setAcceccibleToPublic(Boolean acceccibleToPublic) {
		this.acceccibleToPublic = acceccibleToPublic;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public List<PlanSection> getPlanSectionList() {
		return planSectionList;
	}

	public void setPlanSectionList(List<PlanSection> planSectionList) {
		this.planSectionList = planSectionList;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Override
	public String toString() {
		return "plan{" + "id=" + id + ", name='" + name + '\'' + ", acceccibleToPublic=" + acceccibleToPublic + ", userId=" + userId
				+ ", planSectionList=" + planSectionList + '}';
	}
}
