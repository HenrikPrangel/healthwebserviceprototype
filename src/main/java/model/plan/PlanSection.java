package model.plan;

import java.util.List;

public class PlanSection {

	private Integer sectionId;
	private Integer planId;
	private String sectionName;
	private List<PlanDay> planDayList;

	public PlanSection() {
	}

	public PlanSection(Integer sectionId, Integer planId, String sectionName) {
		this.sectionId = sectionId;
		this.planId = planId;
		this.sectionName = sectionName;
	}

	public Integer getSectionId() {
		return sectionId;
	}

	public void setSectionId(Integer sectionId) {
		this.sectionId = sectionId;
	}

	public Integer getPlanId() {
		return planId;
	}

	public void setPlanId(Integer planId) {
		this.planId = planId;
	}

	public String getSectionName() {
		return sectionName;
	}

	public void setSectionName(String sectionName) {
		this.sectionName = sectionName;
	}

	public List<PlanDay> getPlanDayList() {
		return planDayList;
	}

	public void setPlanDayList(List<PlanDay> planDayList) {
		this.planDayList = planDayList;
	}

	@Override
	public String toString() {
		return "PlanSection{" + "sectionId=" + sectionId + ", planId=" + planId + ", sectionName='" + sectionName + '\''
				+ ", planDayList=" + planDayList + '}';
	}
}
