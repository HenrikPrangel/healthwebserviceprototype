package model.plan;

import model.plan.enums.ElementTypes;

public class PlanElement {

	private Integer planElementId;
	private ElementTypes elementType;
	private Integer elementId;
	private String elementName;
	private Double amount;

	public PlanElement() {
	}

	public PlanElement(Integer planElementId, ElementTypes elementType, Integer elementId, String elementName, Double amount) {
		this.planElementId = planElementId;
		this.elementType = elementType;
		this.elementId = elementId;
		this.elementName = elementName;
		this.amount = amount;
	}

	public Double getAmount() {
		return amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	public Integer getPlanElementId() {
		return planElementId;
	}

	public void setPlanElementId(Integer planElementId) {
		this.planElementId = planElementId;
	}

	public ElementTypes getElementType() {
		return elementType;
	}

	public void setElementType(ElementTypes elementType) {
		this.elementType = elementType;
	}

	public Integer getElementId() {
		return elementId;
	}

	public void setElementId(Integer elementId) {
		this.elementId = elementId;
	}

	public String getElementName() {
		return elementName;
	}

	public void setElementName(String elementName) {
		this.elementName = elementName;
	}
}
