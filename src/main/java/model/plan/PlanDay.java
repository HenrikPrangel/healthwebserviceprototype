package model.plan;

import model.plan.enums.Days;

import java.util.List;

public class PlanDay {

	private Integer planDayId;
	private Integer planDaySectionId;
	private Days day;
	private List<DayTimeFrame> dayTimeFrameList;

	public PlanDay() {
	}

	public PlanDay(Integer planDayId, Integer planDaySectionId, Days day) {
		this.planDayId = planDayId;
		this.planDaySectionId = planDaySectionId;
		this.day = day;
	}

	public Integer getPlanDayId() {
		return planDayId;
	}

	public void setPlanDayId(Integer planDayId) {
		this.planDayId = planDayId;
	}

	public Integer getPlanDaySectionId() {
		return planDaySectionId;
	}

	public void setPlanDaySectionId(Integer planDaySectionId) {
		this.planDaySectionId = planDaySectionId;
	}

	public Days getDay() {
		return day;
	}

	public void setDay(Days day) {
		this.day = day;
	}

	public List<DayTimeFrame> getDayTimeFrameList() {
		return dayTimeFrameList;
	}

	public void setDayTimeFrameList(List<DayTimeFrame> dayTimeFrameList) {
		this.dayTimeFrameList = dayTimeFrameList;
	}
}
